#!/usr/bin/env python
# (c) HaLKo  lmk, osmo
import bluetooth
import RPi.GPIO as GPIO
import sys
import logging as log
from time import sleep

pin_out = 12
host = "" # BT host controller addr
port = 1 # RasPi bluetooth port
server = None

def print_pinout(led_val):
    print(f' \x1b[1;{30 + led_val}m{led_val:d}\x1b[m', end='\r')

if __name__ == '__main__':

    log.basicConfig(level=log.DEBUG, format="%(message)s")

    # setup GPIO
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin_out, GPIO.OUT)
    # set output led on, to show the script is live
    GPIO.output(pin_out, GPIO.HIGH)

    # setup Bluetooth server
    server = None
    client = None
    try:
        log.debug("Bind to BT server")
        server = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
        server.bind((host, port))

        # wait for BT client
        log.info("Waiting for Bluetooth client...")
        server.listen(1) # one connection at a time
        client, address = server.accept()

        log.info("Client connected")
        log.info("Send bluetooth data to toggle LED on pin %i" % pin_out)

        # led value is stored to get previous state in main loop.
        # booleans True, False correspond to GPIO.HIGH and GPIO.LOW
        led_val = True

        # main loop
        while True:
            # set output pin value
            GPIO.output(pin_out, led_val)

            # print value to console
            print_pinout(led_val)

            # change value on every received data
            data = client.recv(1024)
            log.debug(data)
            led_val = not led_val

            # reduce cpu load
            sleep(1 / 25)

    except KeyboardInterrupt:
        pass

    except Exception as err:
        log.error(err)

    finally:
        GPIO.cleanup()
        if client:
            client.close()
        if server:
            server.close()

