#!/usr/bin/env python

import RPi.GPIO as GPIO
from time import sleep

pin = 12

if __name__ == '__main__':
    print("Blink LED")

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin, GPIO.OUT)

    while True:
        GPIO.output(pin, GPIO.HIGH)
        sleep(0.2)
        GPIO.output(pin, GPIO.LOW)
        sleep(0.2)


