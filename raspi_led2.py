#!/usr/bin/env python
import RPi.GPIO as GPIO
from time import sleep

pin_in = 22
pin_out = 12

if __name__ == '__main__':

    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin_in, GPIO.IN)
    GPIO.setup(pin_out, GPIO.OUT)

    last_val = False

    while True:
        btn_val = not GPIO.input(pin_in)
        GPIO.output(pin_out, btn_val)

        # print value for debugging
        if btn_val != last_val:
            last_val = btn_val
            print(f' \x1b[1;{31 + btn_val}m{btn_val:d}\x1b[m', end='\r')

        sleep(1 / 50)


